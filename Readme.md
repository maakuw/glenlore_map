# How to run this repository
You'll need Node.js, to run ANYTHING, so let's start with that part, before we dive too deep! To check if you have it (and you should as a respectable developer) run the following command in the root folder of the repository once you've cloned it.

> `npm`

If you get something along the lines of..

> `npm <command>`
>
> `Usage:`
> 
> `npm install        install all the dependencies in your project...`

..and that's how you know you have it already. If so, you can skip down to **"Deploying to the BitBucket Repo"**

---

## Installing Node.js
According to the Node developers, Node.js "can be installed in different ways" and they go into *great detail* about the various ways [on the Nodejs.dev website](https://nodejs.dev). The best way *I've found*, after 10+ years stumbling around MacOS, is to use the platforms page here:

**Official packages for all the major platforms**
[Click here to find your platform](https://nodejs.dev/download/)

If you can't find anything that works for you there, you an always [check the  ther package managers here](https://nodejs.dev/download/package-manager/)

---

## Deploying to the BitBucket Repo
In horder to deploy on Mac to BitBucket, you'll need an SSH key. Details on this can be found here: https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/
1. On either OS use the command line to navigate into the base folder.
2. Type "ssh-keygen" and skip through the prompts. You're welcome to enter all the details, but they aren't necessary.
3. Double-check that the files are present using either "dir .ssh" or "ls ~/.ssh" (depending on Os)
4. Since this is a glbal setting, changes are being made to your User and that's where we're storing the deets... GLOBALLY

On Mac, it should look something like this:
>   `ssh-keygen`
>   `Generating public/private rsa key pair.`
>   `Enter file in which to save the key (/c/Users/your_username/.ssh/id_rsa):`
>   `Created directory '/c/Users/your_username/.ssh'.`
>   `Enter passphrase (empty for no passphrase):`
>   `Enter same passphrase again:`
>   `Your identification has been saved in /c/Users/your_username/.ssh/id_rsa.`
>   `Your public key has been saved in /c/Users/your_username/.ssh/id_rsa.pub.`
>   `The key fingerprint is: e7:94:d1:a3:02:ee:38:6e:a4:5e:26:a3:a9:f4:95:d4 your_username@EMMA-PC`

---

## Now that you're set up...
All we need to do is run `npm run serve` to run a Node.js server and trigger a new window in your default browser.

---

## What's all this Tailwind stuff then?
We use Tailwindcss classes and styles to create the structure and visual styling of the application. Since it's already set up, you don't need to run tailwind. If you want to make any changes, run `npm run tailwind` before you start editing. It will republish the core.css file as you make meaningful changes to the DOM or JS files.

## Known Issues
### Docker
Sometimes, after running several versions of Docker, you'll get some DB crashes. These errors can be fixed with the easy steps found here:
https://stackoverflow.com/questions/67636378/laravel-sail-on-existing-project-unknown-database-sqlstatehy000-1049/67640973#67640973

If you're running Docker, and you have multiple services or the environment variables change at all, services will often fail. Here are the 2 best ways to reset your system and get you back going:
1) docker system prune — this command will get rid of existing "containers" Docker has on-deck for your projects so that your information is saved and it's quick to start up. Sadly, changes to your env or modules sometimes throws this "system" off. You'll lose your structure, but you'll be able to restart after this npm command
2) docker volume prune — same as the system prune, only for "Volumes". This is data, not structure so ensure you have a backup if you need your old info

### Sail
If, at some point, you've been manually screwing around with the Seeders, Factories, or Migrations, it's a good bet you've screwed up the migration by now. If you run into a wall, try these steps:
1) sail composer dump-autoload -o — re-align the autoload to your changes. Doesn't always work, but it's the first step. Next, if that doesn't work, ensure that Laravel didn't put all your "seeders" in a "seeds" directory. Not sure why this misnaming happens, I believe it to be a legacy effect.
2) sail artisan route:clear — purge out the old paths and re-evaluate the routes outlined by composer even if they're new
3) sail artisan cache:clear — same as above, only with the cache