<?php

namespace App\Events;

use App\Models\Requests;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SosTriggered implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $lat,$lng,$time,$id,$responded;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Requests $req)
    {
        $this->id = $req->id;
        $this->lat = $req->lat;
        $this->lng = $req->lng;
        $this->responded = $req->responded;
        $this->time = $req->time;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('requests');
    }
}
