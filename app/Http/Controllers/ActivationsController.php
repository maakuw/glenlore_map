<?php

namespace App\Http\Controllers;

use App\Models\Activations;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ActivationsController extends Controller {
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $activations = Activations::all();
    return response()->json($activations);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $request->validate([
      'lat' => 'required',
      'lng' => 'required'
    ]);

    $activations = new Activations([
      'lat' => $request->get('lat'),
      'lng' => $request->get('lng'),
      'dot' => $request->get('dot'), 
      'title' => $request->get('title'), 
      'description' => $request->get('description'), 
      'anchorLat' => $request->get('anchorLat'),
      'anchorLng' => $request->get('anchorLng'), 
    ]);

    $activations->save();

    return response()->json($activations);
  }

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
  public function update(Request $request, $id)
  {
    $request->validate([
      'lat' => 'required',
      'lng' => 'required'
    ]);

    $activations = Activations::find($id);
    $activations->lat = $request->get('lat');
    $activations->lng = $request->get('lng');
    $activations->dot = $request->get('dot');
    $activations->title = $request->get('title');
    $activations->description = $request->get('description');
    $activations->anchorLat = $request->get('anchorLat');
    $activations->anchorLng = $request->get('anchorLng');

    $activations->save();
    return response()->json('Activation successfully updated!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $activations = Activations::find($id);
    $activations->delete();
    
    return response()->json('Activation successfully deleted!');
  }
}