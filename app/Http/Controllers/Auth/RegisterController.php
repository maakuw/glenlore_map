<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

use App\Models\User;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::SUCCESS;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required|string|max:255'],
            'email' => ['required|string|email|max:255|unique:users'],
            'company' => ['string|min:2|max:120'],
            'title' => ['string|min:2|max:32'],
            'bio' => ['string|max:500'],
            'address' => ['string|max:500'],
            'first_name' => ['required|string|min:2|max:24'], 
            'last_name' => ['min:2|max:24'], 
            'password' => ['required', 'confirmed', Rules\Password::defaults()]
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'], 
            'username' => substr($data['first_name'],1) . $data['last_name'], 
            'email' => $data['email'],
            'address' => $data['address'], 
            'company' => $data['company'], 
            'title' => $data['title'],
            'bio' => $data['notes'],
            'user_level_id' => 3, 
            'password' => Hash::make($request->password), 
            'photo' => '//via.placeholder.com/300x300'
        ]);

        event(new Registered($user));

        Auth::login($user);

        $goto = $user->user_level_id === 3 ? RouteServiceProvider::HOME : '/admin';

        return redirect($goto);
    }
}
