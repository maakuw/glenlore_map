<?php
 
namespace App\Http\Controllers;

use App\Events\SosTriggered;
use App\Http\Controllers\Controller;
use App\Models\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Broadcast;

class RequestsController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $soss = Requests::all();
        return response()->json($soss);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'lat' => 'required',
            'lng' => 'required'
        ]);

        $sos = new Requests([
            'lat' => $request->get('lat'),
            'lng' => $request->get('lng'),
            'time' => date_format(date_create(),'m/d h:i a'),
            'responded' => false
        ]);

        $sos->save();

        SosTriggered::dispatch($sos);

        return response()->json($sos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'lat' => 'required',
            'lng' => 'required'
        ]);

        $sos = Requests::find($id);
        $sos->lat = $request->get('lat');
        $sos->lng = $request->get('lng');
        $sos->time = date_format(date_create(),'m/d h:i a');
        $sos->responded = $request->get('responded');

        $sos->save();
        $soss = Requests::all();
        return response()->json($soss);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sos = Requests::find($id);
        $sos->delete();
        
        $soss = Requests::all();
        return response()->json($soss);
    }
}