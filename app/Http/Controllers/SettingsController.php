<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Settings;

use App\Events\SettingsUpdated;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::all();

        return response()->json($settings);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
        $user = Auth::user();
        if(!$user) $user = false;

        return response()->json($user);
    }

    /**
     * Display the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $setting = Settings::find($request->get('id'));

        return response()->json($setting);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->middleware('auth');

        $settings = Settings::all();
        foreach($settings as $s => $setting){
            $setting->value = $request->get($s)['value'];
            $setting->updated_at = $request->get($s)['updated_at'];
            $setting->save();
        }

        SettingsUpdated::dispatch($settings);

        return response()->json($settings);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();
    
        $request->session()->invalidate();
    
        $request->session()->regenerateToken();
    
        return redirect('/login');
    }
}
