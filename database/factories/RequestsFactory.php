<?php

namespace Database\Factories;

use App\Models\Requests;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class RequestsFactory extends Factory
{
  /**
  * Define the model's default state.
  *
  * @return array
  */
  protected $model = Requests::class;

  public function definition()
  {
    $faker = Faker::create();
    return [
      'lat' => $faker->latitude(42.5878,42.5750),
      'lng' => $faker->longitude(-83.4873,-83.4675),
      'time' => $faker->time('m/d h:i a')
    ];
  }
}
