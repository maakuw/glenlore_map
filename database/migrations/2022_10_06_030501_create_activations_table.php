<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->decimal('lat',20,6);
            $table->decimal('lng',20,6);
            $table->decimal('anchorLat',2,0)->default(0);
            $table->decimal('anchorLng',2,0)->default(0);
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('dot')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activations');
    }
}
