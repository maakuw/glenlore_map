<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivationsTableSeeder extends Seeder {
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run() {
    DB::table('activations')->insert([
      "dot" => config('app.fb_url_prefix', './images/') . "icon__start.svg" . config('app.fb_url_suffix', ''),
      "anchorLat" => 25,
      "anchorLng" => 60,
      "lat" => 42.57962, 
      "lng" => -83.47467044570127
    ]);
    DB::table('activations')->insert([
      "title" => 'Trail Entrance', 
      "lat" => 42.57923898845562,
      "lng" => -83.4756219578406,
      "description" => "Welcome to Aurora, an immersive wintertime experience for all ages!"
    ]);
    DB::table('activations')->insert([
      "title" => 'Snow Globe Garden', 
      "lat" => 42.579017785383556,
      "lng" => -83.4756434155112,
      "description" => "Take a snow globe for a spin – what holiday magic will you reveal?"
    ]);
    DB::table('activations')->insert([
      "title" => 'Evergreen Passage', 
      "lat" => 42.5788597827088,
      "lng" => -83.4763837051502,
      "description" => "A pine tree path of rhythmic light, perfect for a winter’s night."
    ]);
    DB::table('activations')->insert([
      "dot" => config('app.fb_url_prefix', './images/') . "icon__restroom.svg" . config('app.fb_url_suffix', ''),
      "anchorLat" => 0,
      "anchorLng" => 20,
      "lat" => 42.580491854762876,
      "lng" => -83.47798169231007
    ]);
    DB::table('activations')->insert([
      "title" => 'Cane Storm', 
      "lat" => 42.579073086225144,
      "lng" => -83.4764802646683,
      "description" => "There’s a candy cane blizzard in the gingerbread forest!"
    ]);
    DB::table('activations')->insert([
      "title" => 'Ginger Puns', 
      "lat" => 42.57938119001615,
      "lng" => -83.47695233342354,
      "description" => "What do you get when you cross a gingerbread man with a comedian? A wisecracker!"
    ]);
    DB::table('activations')->insert([
      "dot" => config('app.fb_url_prefix', './images/') . "icon__restroom.svg" . config('app.fb_url_suffix', ''),
      "anchorLat" =>  12,
      "anchorLng" => 10,
      "lat" => 42.58061959228234,
      "lng" => -83.47603732446747
    ]);
    DB::table('activations')->insert([
      "title" => 'Forest Bells', 
      "lat" => 42.58021069265572,
      "lng" => -83.47781064025125,
      "description" => "Step on the spinning snowflakes and trigger a merry tune."
    ]);
    DB::table('activations')->insert([
      "title" => 'Gingerbread Boogie', 
      "lat" => 42.5805424906217,
      "lng" => -83.47787501326333,
      "description" => "If this gingerbread house is a rockin, don’t come a knockin!"
    ]);
    DB::table('activations')->insert([
      "title" => 'The Sequence', 
      "lat" => 42.58075578838176,
      "lng" => -83.47847582804272,
      "description" => "Place the glowing ornaments to transform the melody."
    ]);
    DB::table('activations')->insert([
      "title" => 'Radiant Lights', 
      "lat" => 42.58111128302699,
      "lng" => -83.478980083304,
      "description" => "Dazzling lights paired with a playful take on a holiday classic."
    ]);
    DB::table('activations')->insert([
      "title" => 'The Festive Field', 
      "lat" => 42.582335749068484,
      "lng" => -83.4795594404127,
      "description" => "Enter a path of light and wonder, while keeping a watchful eye on the sky."
    ]);
    DB::table('activations')->insert([
      "title" => 'Frosted Windows', 
      "lat" => 42.583173111999336,
      "lng" => -83.47871186242033,
      "description" => "Wave your hands and reveal a frosty field."
    ]);
    DB::table('activations')->insert([
      "title" => 'Snow Globe Serenade', 
      "lat" => 42.583868272681016,
      "lng" => -83.47769262306244,
      "description" => "These carolers are full of cheer, they’re just so glad you’re finally here!"
    ]);
    DB::table('activations')->insert([
      "title" => 'Caroling Trees', 
      "lat" => 42.584018363174366,
      "lng" => -83.47697379109424,
      "description" => "Glenlore’s trees won’t be outdone, it’s time for them to have some fun!"
    ]);
    DB::table('activations')->insert([
      "dot" => config('app.fb_url_prefix', './images/') . "icon__restroom.svg" . config('app.fb_url_suffix', ''),
      "anchorLat" => 12,
      "anchorLng" => 10,
      "lat" => 42.583961169225745,
      "lng" => -83.47737842888574
    ]);
    DB::table('activations')->insert([
      "title" => 'Nick\'s Snacks', 
      "dot" => config('app.fb_url_prefix', './images/') . "icon__snack_bar.svg" . config('app.fb_url_suffix', ''),
      "lat" => 42.58401046368373,
      "lng" => -83.47630860330277,
      "description" => "Time for a drink and a snack and…who’s that I hear? Could it be that Santa’s near?"
    ]);
    DB::table('activations')->insert([
      "title" => 'St. Nick\'s Castle', 
      "lat" => 42.583181011596075,
      "lng" => -83.47643734932691,
      "description" => "See Glenlore’s elves at work and play!"
    ]);
    DB::table('activations')->insert([
      "title" => 'Celestial Clearing', 
      "lat" => 42.582754431939726,
      "lng" => -83.47653390884504,
      "description" => "These paths of light follow your movements. Gather some friends and form a constellation!"
    ]);
    DB::table('activations')->insert([
      "title" => 'Crystal Tunnel', 
      "lat" => 42.58240684635801,
      "lng" => -83.47667338370454,
      "description" => "Our twisted tunnel is all decked out for the festive holiday season."
    ]);
    DB::table('activations')->insert([
      "title" => 'The Tranquil Path', 
      "lat" => 42.581988161152594,
      "lng" => -83.47639443398555,
      "description" => "Relaxing tunes and floral lights, illuminating winter nights."
    ]);
    DB::table('activations')->insert([
      "title" => 'VibraCanes', 
      "lat" => 42.58109548330804,
      "lng" => -83.4763407898088,
      "description" => "The Glenlore elves just love this candy cane vibraphone!"
    ]);
    DB::table('activations')->insert([
      "title" => 'Snowball Fight', 
      "lat" => 42.58055829048081,
      "lng" => -83.47591163639497,
      "description" => "How many snowmen can you knock down? Can you conquer the frozen giant?"
    ]);
    DB::table('activations')->insert([
      "title" => 'Balloon Parade', 
      "lat" => 42.58025019250617,
      "lng" => -83.47587944988891,
      "description" => "These uplifting floats end the trail on a high note!"
    ]);
    DB::table('activations')->insert([
      "title" => 'Adieu', 
      "lat" => 42.58013959286187,
      "lng" => -83.47535373695695,
      "description" => "Thanks for sharing another Glenlore season. Hope to see you soon!"
    ]);
    DB::table('activations')->insert([
      "dot" => config('app.fb_url_prefix', './images/') . "icon__restroom.svg" . config('app.fb_url_suffix', ''),
      "anchorLat" =>  -20,
      "anchorLng" => 0,
      "lat" => 42.580135806738475,
      "lng" => -83.4748540864353
    ]);
    DB::table('activations')->insert([ 
      "dot" => config('app.fb_url_prefix', './images/') . "icon__end.svg" . config('app.fb_url_suffix', ''),
      "anchorLat" =>  24,
      "anchorLng" => 30,
      "lat" => 42.58025,
      "lng" => -83.4746
    ]);
    DB::table('activations')->insert([
      "dot" => config('app.fb_url_prefix', './images/') . "icon__restroom.svg" . config('app.fb_url_suffix', ''),
      "anchorLat" =>  12,
      "anchorLng" => 10,
      "lat" => 42.57904492353843,
      "lng" => -83.47510100268856
    ]);
  }
}
