<?php

namespace Database\Seeders;

use App\Models\Requests;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestsTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
      Requests::factory()->count(5)->create();
    }
  }
  