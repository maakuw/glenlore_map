<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'key' => 'version',
            'title' => 'Application Version', 
            'description' => 'This number iterates along with the GitHub Release Number',
            'value' => '2.3.5'
        ]);
        DB::table('settings')->insert([
            'key' => 'title',
            'title' => 'Page Title', 
            'description' => '',
            'value' => "Glenlore Map | Experience the wonder &amp; the magic of Glenlore Trails!"
        ]);
        DB::table('settings')->insert([
            'key' => 'description',
            'title' => 'Page Description', 
            'description' => 'This first part of the tagline, appearing in the upper right hand side of the Header on the Screen view, is in bold font',
            'value' => "Glenlore Map | Aurora : This web application both showcases the location of the Glenlore Trails experience, as well as triggering an SOS for people caught in trouble."
        ]);
        DB::table('settings')->insert([
            'key' => 'instagram',
            'title' => 'Instagram Handle', 
            'description' => 'Optional link to Instagram, appearing in the center of the Footer on the Screen view (unused)',
            'value' => ''
        ]);
        DB::table('settings')->insert([
            'key' => 'midground',
            'title' => 'Midground Image', 
            'description' => 'The fixed image which appears over-top of the map, below the Foreground Image',
            'value' => 'topography.svg',
            'type' => 'image'
        ]);
        DB::table('settings')->insert([
            'key' => 'foreground',
            'title' => 'Foreground Image', 
            'description' => 'The fixed image which appears over-top of the map, below the map Markers',
            'value' => 'walking_path.svg',
            'type' => 'image'
        ]);
        DB::table('settings')->insert([
            'key' => 'logo',
            'title' => 'Client Logo', 
            'description' => 'Logo, appearing in the center of the Header',
            'value' => 'logo__campaign.png',
            'type' => 'image'
        ]);
        DB::table('settings')->insert([
            'key' => 'logo_alt',
            'title' => 'Client Logo Alt Text', 
            'description' => 'This second part of the tagline, appearing in the upper right hand side of the Header on the Screen view, is in normal font',
            'value' => "Aurora: Glenlore Trails"
        ]);
        DB::table('settings')->insert([
            'key' => 'primary',
            'title' => 'Primary Color', 
            'description' => 'Used for the Header and Footer text, hover color on on table rows, Admin Sidebar menu, small buttons, and "Enabled" Countries on the Map',
            'value' => '#6666FF',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'secondary',
            'title' => 'Secondary Color', 
            'description' => 'Used for Table headers, Globally for anchors, labels, large buttons, and "Selected" Countries on the Map',
            'value' => '#73D0FF',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'tertiary',
            'title' => 'Tertiary Color', 
            'description' => 'Used for the Navigation panels, Table borders, and anything that is a focal color but NOT a link',
            'value' => '#CC66FF',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'overlay',
            'title' => 'Overlay Background Color', 
            'description' => 'Used for the Navigation panels, Table borders, and anything that is a focal color but NOT a link',
            'value' => '#73D0FF',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'overlay',
            'title' => 'Overlay Icon Color', 
            'description' => 'Used for the Navigation panels, Table borders, and anything that is a focal color but NOT a link',
            'value' => '#FFFFFF',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'overlay',
            'title' => 'Overlay Text Color', 
            'description' => 'Used for the Navigation panels, Table borders, and anything that is a focal color but NOT a link',
            'value' => '#FFFFFF',
            'type' => 'color'
        ]);
        DB::table('settings')->insert([
            'key' => 'header',
            'title' => 'Header Background Color', 
            'description' => 'Used for the top Header bar.',
            'value' => '#6666FF',
            'type' => 'color'
        ]);
    }
}