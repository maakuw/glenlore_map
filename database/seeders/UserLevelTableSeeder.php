<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_level')->insert([
            'name' => 'Administrator',
            'slug' => 'administrator',
            'level' => 1
        ]);
        DB::table('user_level')->insert([
            'name' => 'Volunteer',
            'slug' => 'volunteer',
            'level' => 2
        ]);
    }
}
