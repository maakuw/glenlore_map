<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('users')->insert([
      "username" => "MaakuW",
      "email" => "mwilliamson@bluewatertech.com",
      'password' => bcrypt('m1Ch!9@N'),
      'user_level_id' => 1
    ]);
    DB::table('users')->insert([
      "username" => "Andrew L",
      "email" => "alorenz@bluewatertech.com",
      'password' => bcrypt('B1u3W@t3R'),
      'user_level_id' => 1
    ]);
    DB::table('users')->insert([
      "username" => "Myself", 
      "first_name" => "Mark",
      "last_name" => "Lorenz", 
      "email" => "mlorenz@bluewatertech.com",
      'password' => bcrypt('B1u3W@t3R'),
      'user_level_id' => 2
    ]);
    DB::table('users')->insert([
      "username" => "Warda S", 
      "first_name" => "Sheikh",
      "last_name" => "Lorenz", 
      "email" => "wsheikh@bluewatertech.com",
      'password' => bcrypt('B1u3W@t3R'),
      'user_level_id' => 2
    ]);
    DB::table('users')->insert([
      "username" => "Chanel S", 
      "first_name" => "Chanel",
      "last_name" => "Schoeneneberger", 
      "email" => "cschoeneberger@bluewatertech.com",
      'password' => bcrypt('B1u3W@t3R'),
      'user_level_id' => 2
    ]);
  }
}
