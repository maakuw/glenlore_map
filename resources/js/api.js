import axios from 'axios'
import { initializeApp } from "firebase/app"
import { getAnalytics } from "firebase/analytics"
import { getMessaging, getToken, onMessage } from "firebase/messaging"

/*
export const firebaseConfig = {
  apiKey: "AIzaSyC_oV4ddKanSbvjLdHyWbzcYpyYiuzsNGs",
  authDomain: "glenlore-map.firebaseapp.com",
  projectId: "glenlore-map",
  storageBucket: "glenlore-map.appspot.com",
  messagingSenderId: "252276861997",
  appId: "1:252276861997:web:e7a78b9ce4252483711ca4",
  measurementId: "G-K7Z9F0022Y"
}
*/
//export const topic = 'sos'

//export const app = initializeApp(firebaseConfig)

// Initialize Firebase Cloud Messaging and get a reference to the service
//export const messaging = getMessaging()
/*export const sendmsg = async(message) => {
  await getMessaging()
  .try(send(message)
  .then((response) => {
    // Response is a message ID string.
    console.log('Successfully sent message:', response)
  })
  .catch((error) => {
    console.log('Error sending message:', error)
  })).
  catch((error) => {
    console.log('Error with the "Send" protocol:', error)
  })
}

export const analytics = getAnalytics(app)

getToken(messaging, {vapidKey:"BEpprc32EFaO1JoBSZvkMz-a38GHMofkyh1haEWvosATiolsoDwAJ0TeRi4ct6yS6WggscPe8-YE0WJBkGB-x74"}).then((currentToken) => {
  if (currentToken) {
    // Send the token to your server and update the UI if necessary
    console.log('Token received! We can send messages', currentToken)
    // ...
  } else {
    // Show permission request UI
    console.log('No registration token available. Request permission to generate one.');
    // ...
  }
}).catch((err) => {
  console.log('An error occurred while retrieving token. ', err);
  // ...
})
onMessage(messaging, (payload) => {
  console.log('Message received. ', payload);
  // ...
})*/

export default axios.create({
  baseURL: '//'+window.location.host
})
/*
export const postTracking = (user) => {
  const xhr = new XMLHttpRequest()
  let token = document.querySelector('meta[name="csrf-token"]')
      
  xhr.onload = () => {
    if (xhr.status == 200) {
      user.tracking = JSON.parse(xhr.response)
    } else {
      console.error('Error!')
    }
  }
  
  xhr.open('PUT', '/resource/tracking/'+user.tracking_id, true)

  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
  xhr.setRequestHeader('X-CSRF-TOKEN', token.content), 
  xhr.setRequestHeader('content-type', 'application/json')

  xhr.send(JSON.stringify(user.tracking))
}

export const trackIt = (event, user) => {
  let tracking = event.target.dataset.tracking
  if(tracking) analytics.logEvent(tracking)
  if(user) {
    let trackable = false

    Object.keys(user.tracking).map((evt) => {
      if(evt === tracking){
        trackable = evt
      }
    })

    if(trackable){
      user.tracking[trackable] += 1
      postTracking(user)
    }
  }
}*/

//After a specified time, close the Alert component
//Dev Note: Not sure why this is here? Deprecated perhaps?
export const timedAlert = (callback = false, time = 3600) => {
  var alerts = document.querySelector('[data-alert]')

  function animateOut(callback) { 
    callback && callback()
    alerts.classList.remove('show')
  }

  if(alerts){
    //Reset the animator and the Alert
    alerts.classList.add('show')

    if(callback){
      setTimeout(() => animateOut(callback), time)
    }else{
      setTimeout(() => animateOut(), time)
    }
  }else{
    return {
      feedback: {
        msg: 'timedAlert called, but no alerts are avialable to close', 
        style: 'bg-primary text-white'
      }
    }
  }
}