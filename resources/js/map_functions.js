import api/*, { topic, sendmsg, trackIt, analytics, app, messaging }*/ from './api'
import Sidebar from './Sidebar.vue'
import Topbar from './Topbar.vue'
import {createApp} from 'vue'

const FIREBASE_URL_PREFIX = 'https://firebasestorage.googleapis.com/v0/b/glenlore-map.appspot.com/o/'
const FIREBASE_URL_SUFFIX = '?alt=media'

const bypass_gate = false
let mapID = document.querySelector("meta[name=map-id]")
let is_holding = false
let locationButton, modalHelp, has_user, sosButton, sosSubmit, spaceOverlay, pathOverlay, ring, 
activations = [], markers = [], infowindow, 
map, mapCenterLG = {lat: 42.5818, lng: -83.478}, mapCenter = {lat: 42.5797, lng: -83.4770}, 
boundaries = {
  north: 42.5878,
  south: 42.5750,
  east: -83.4675,
  west: -83.4873
},
mapZoom = 16.5,  
standardOptions = {
  zoom: mapZoom,
  minZoom: bypass_gate ? false : mapZoom - (window.innerWidth/1440),
  maxZoom: bypass_gate ? false : mapZoom + (window.innerWidth/1440),
  tilt: 0, 
  zoomControl: false,
  rotateControl: false,
  tiltControl: false,
  disableDefaultUI: true, 
  center: window.innerWidth > 960 ? mapCenterLG : mapCenter, 
  mapId: mapID ? mapID.content : false,
  restriction: bypass_gate ? null :  {
    latLngBounds: boundaries,
  }
}

const spaceCoors = {
  north: 42.593,
  south: 42.5698,
  east: -83.4665,
  west: -83.4895
}

const pathCoors = {
  north: 42.5841,
  south: 42.5780,
  east: -83.4732,
  west: -83.4810
}

function hideModal(modal, callback=false){
  modal.classList.remove('opacity-100')
  setTimeout(() => {
    modal.classList.add('hidden')
    if(callback) callback()
  }, 1000)
}

function showModal(modal, callback=false, title=false, msg=false){
  let h2 = modal.querySelector('h2')
  let p = modal.querySelectorAll('p')
  if(p){
    if(title) h2.innerHTML = title
    if(msg) {
      p[0].innerHTML = msg
      if(p[1]) p[1].classList.add('hidden')
    }else{
      if(p[1]) p[1].classList.remove('hidden')
    }
  }
  modal.classList.remove('hidden')
  setTimeout(() => {
    modal.classList.remove('opacity-0')
    modal.classList.add('opacity-100')
    if(callback) callback()
  }, 300)
}
/*
function getResolution() {
  let size = window.innerWidth
  let newSize = ''
  if(size > 960){
    newSize = 'desktop'
  }else if(size <= 960 && size > 620){
    newSize = 'tablet'
  }else{
    newSize = 'mobile'
  }
  return newSize
}
*/
function setProgress(percent, callback=false){
  if(ring){
    const radius = ring.r.baseVal.value
    const circumference = radius * 2 * Math.PI
    const offset = circumference - percent / 100 * circumference
    ring.style.strokeDashoffset = offset
  }
  let count = 0
  let progress = setInterval(() => {
    if(count === 10 || !is_holding){
      clearInterval(progress)
      count = 0
      if(is_holding){
        if(callback) callback()
      }else{
        ring.style.strokeDashoffset = ring.style.strokeDasharray
      }
    }else{
      count++
    }
  }, 100)
}

function triggerSos(event=false){
  if (navigator.geolocation) {

    setProgress(100, () => {
      let modal = document.querySelector("#modal__help")
      let form = modal.querySelector("form")
      let msgs = form.querySelectorAll('legend > div')
      let smiley = document.getElementById('smiley')
      let exclaim = document.getElementById('exclamation')
      let action_lines = document.getElementById('action_lines')
      let ring_wrap = document.querySelector('[data-progress]')
      let left_eye = document.getElementById('smiley__eye--l')
      let right_eye = document.getElementById('smiley__eye--r')
      let smile = document.querySelector('#smile_clip circle')

      function resetHelp() {
        ring.style.strokeDashoffset = ring.style.strokeDasharray
        is_holding = false
        
        if(msgs.length){
          msgs.forEach(msg => {
            msg.classList.toggle('hidden')
          })
        }
        exclaim.classList.remove('opacity-0')
        exclaim.removeAttribute('style')
        ring_wrap.classList.remove('scale-150', 'opacity-0')
        if(left_eye) left_eye.classList.add('hidden')
        if(right_eye) right_eye.classList.add('hidden')
        if(action_lines) action_lines.classList.add('hidden')
        if(smile) smile.removeAttribute('style')
      }

      if(msgs.length){
        if(action_lines){
          setTimeout(() => {
            action_lines.classList.remove('scale-0')
            action_lines.classList.add('opacity-100', 'scale-100')
          }, 100)
        }
        ring_wrap.classList.add('scale-150', 'opacity-0')
        exclaim.style.filter = 'blur(1px)'
        exclaim.style.transform = 'scale(1.03) translate(0,5px) rotate(115deg)'
        setTimeout(() => {
          exclaim.classList.remove('duration-300')
          exclaim.style.filter = ''
          exclaim.style.transform = 'scale(1.03) translate(0,5px) rotate(115deg) rotate(-25deg)'
          setTimeout(() => {
            smiley.classList.remove('hidden')
            setTimeout(() => {
              exclaim.classList.add('opacity-0')
              if(left_eye) left_eye.classList.remove('hidden')
              if(right_eye) right_eye.classList.remove('hidden')
              if(smile) smile.style.transform = 'translate(48px, 50px) scale(80)'
            }, 60)
          }, 600)
        }, 400)
        msgs.forEach(msg => {
          msg.classList.toggle('hidden')
        })
      }

      setTimeout(() => { navigator.geolocation.getCurrentPosition((position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        }

        let in_latitude = pos.lat < boundaries.north && pos.lat > boundaries.south
        let in_longitude = pos.lng < boundaries.west && pos.lat > boundaries.east
        
        if( ( in_latitude && in_longitude ) || bypass_gate ) {
          let locus = document.getElementById('user_location')
          if(locus) {
            locus.value = JSON.stringify(pos)

            if(form) {
              api.post(`/resource/requests`, {
                lat: pos.lat,
                lng: pos.lng,
              })
              .then((response) => {
                console.log(response.data)
              })
              .finally(() => {
                hideModal(modalHelp, resetHelp)
              })
            }
          }
        }
      },(error) => {
        if(bypass_gate){
          api.post(`/resource/requests`, {
            lat: 42.5808,
            lng: -83.477
          })
          .then((response) => {
            console.log('Position can\'t be pulled with "bypass" on. Hard Coded position passed:', response.data)
          })
          .finally(() => {
            hideModal(modalHelp, resetHelp)
          })
        }else{
          handleLocationError(error.code, error.message)
        }
      }) }, 2000)
      
    })
  }

  return false
}

function setHolding(state){
  is_holding = state
}

function handleLocationError(code = false, msg = false, callback = false) {
  let modal = document.getElementById('modal__geolocation_error')

  if(code === 1) {
    console.log(code, msg, callback)
    showModal(modal,callback,'Security Error', msg)
  }else{
    showModal(modal,callback)
  }
}

export function focusMarker(marker,step) {
  const content = '<aside class="w-48 lg:w-52">' +
  '<button class="pointer-events-none absolute z-50 -right-3 -top-3 rounded-full w-8 h-8 text-gray-50 text-lg bg-primary flex align-center content-center justify-center" style="box-shadow: 0px 6px 7px rgba(102, 51, 255, 0.25);">' +
  '&times;' + 
  '</button>' +
  ( step.title ? '<h1 class="text-xs landscape-mobile:text-xxs lg:text-sm font-bold uppercase pb-1 lg:pb-2 leading-none">'+step.title+'</h1>' : '' ) +
  ( step.description ? '<p class="pb-1 lg:pb-2 text-xs landscape-mobile:text-xxxs lg:text-sm leading-tight font-normal">'+step.description+'</p>' : '' ) +
  "</aside>"

  if( !infowindow ){
    infowindow = new google.maps.InfoWindow({
      content: content,
      maxWidth: 312
    })
  }else{
    infowindow.setContent(content)
  }

  infowindow.open({
    anchor: marker,
    map,
    shouldFocus: true
  })
  
  map.setCenter(step.position)
}

function initMap(options) {
  map = new google.maps.Map(document.getElementById("map"), {
    ...options
  })

  activations.forEach(act => {
    act.position = {
      lat: parseFloat(act.lat),
      lng: parseFloat(act.lng)
    }

    if(act.anchorLat & act.anchorLng) act.anchor = new google.maps.Point(act.anchorLat,act.anchorLng)
  })
  
  spaceOverlay = new google.maps.GroundOverlay(
    FIREBASE_URL_PREFIX + "topography.svg" + FIREBASE_URL_SUFFIX,
    spaceCoors
  )

  pathOverlay = new google.maps.GroundOverlay(
    FIREBASE_URL_PREFIX + "walking_path.svg" + FIREBASE_URL_SUFFIX,
    pathCoors
  )

  function makeDot(step){
    if(!step) return false
    return {
      url: step.dot ? step.dot : FIREBASE_URL_PREFIX + 'dot.svg' + FIREBASE_URL_SUFFIX,
      anchor: step.anchor ? step.anchor : new google.maps.Point(18,18)
    }
  }

  has_user        = document.querySelector("meta[name=user]")
  locationButton  = document.querySelector("[data-location]")
  sosButton       = document.querySelector("[data-sos]")
  modalHelp       = document.getElementById('modal__help')

  function getHelp() {
    let modal = modalHelp
    showModal(modal)
  }

  let clone = false
  let locationWindow = new google.maps.InfoWindow()

  function checkLocation(event=false) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        }

        let in_latitude = pos.lat < boundaries.north && pos.lat > boundaries.south
        let in_longitude = pos.lng > boundaries.west && pos.lng < boundaries.east
        
        if( ( in_latitude && in_longitude ) || bypass_gate ) {
        
          if ( event ) {
            if(locationButton){
              clone = locationButton.cloneNode(true)
              map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(clone);
              clone.addEventListener("click", checkLocation)
              locationButton = false
            }

            if(sosButton){
              clone = sosButton.cloneNode(true)
              map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(clone);
              clone.addEventListener("click", getHelp)
              sosButton = false
            }

            clone = false
          }
          locationWindow.setPosition(pos)
          locationWindow.setContent("You are here!")
          locationWindow.open(map)
          map.setCenter(pos)
        }else{
          let modal = document.getElementById('modal__out_of_zone')
          showModal(modal)
        }
      },
      (error) => {
        handleLocationError(error.code, error.message)
        if( bypass_gate && sosButton){
          clone = sosButton.cloneNode(true)
          map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(clone)
          clone.addEventListener("click", getHelp)
          sosButton = false
        }
      })
    } else {
      handleLocationError()
    }
  }

  spaceOverlay.setMap(map)

  let modal = !document.querySelector('body[data-logged]') && document.getElementById('modal__welcome')
  if(modal) {
    let closers = modal.querySelectorAll('[data-close]')

    if(closers) closers.forEach(btn => {
      btn.addEventListener("click", () => {
        hideModal(modal, () => {
          showMap(activations, checkLocation, makeDot, true)
        })
      })
    })

    showModal(modal)
  }else{
    showMap(activations, checkLocation, makeDot, false)
  }
//Dev Note: replace this with a listener... this works, but it's inefficient
  if(document.querySelector('#sidebar')) {
    const sidebar = createApp(Sidebar, {
      map: map, 
      user: JSON.parse(has_user.content),
      fb_prefix: FIREBASE_URL_PREFIX,
      fb_suffix: FIREBASE_URL_SUFFIX
    })
    .mount('#sidebar')
  }
  if(document.querySelector('#topbar')) {
    createApp(Topbar, {
      user: JSON.parse(has_user.content)
    }).mount('#topbar')
    
  }
}

function showMap(activations, checkLocation, makeDot, show_header=true) {

  pathOverlay.setMap(map)
          
  if(!has_user) {
    checkLocation('bypass')
  }

  activations.forEach(step => {
    const marker = new google.maps.Marker({
      title: step.title ? step.title : '', 
      position: step.position,
      map: map,
      icon: makeDot(step)
    })

    markers.push(marker)
    
    if(step.title){
      marker.addListener("click", () => focusMarker(marker,step))
    }
  })

  if(show_header) {
    let header = document.querySelector('main > header')
    if(header) header.classList.remove('hidden')

    let login = document.querySelector('[data-login]')
    if(login) login.classList.remove('lg:hidden')
  }
  map.setHeading(269)
}

document.addEventListener("DOMContentLoaded", () => {
  //console.log(messaging)
  //analytics.logEvent('pageload')

  // Send a message to devices subscribed to the provided topic.
  /*sendmsg({
    data: {
      lat: '850',
      lng: '245'
    },
    topic: topic
  })*/

  ring = document.querySelector('[data-progress] circle')
  if(ring) {
    const radius = ring.r.baseVal.value
    const circumference = radius * 2 * Math.PI

    ring.style.strokeDasharray = circumference
    ring.style.strokeDashoffset = circumference;
  }

  if(typeof google != 'undefined') {
    api.get(`/resource/activations`)
    .then((response) => activations = response.data)
    .catch((error) => console.log(error))
    .finally(() => initMap(standardOptions))
  }

  let modals = document.querySelectorAll('[data-modal]')
  let closeBtns = document.querySelectorAll('[data-modal] [data-close]')

  if(closeBtns) closeBtns.forEach(btn => {
    btn.addEventListener("click", () => {
      modals.forEach(modal => {
        hideModal(modal)
      })
    })
  })

  function setSos(event){
    setHolding(true)
    triggerSos(event)
  }

  function unsetSos(){
    setHolding(false)
  }
  sosSubmit       = document.querySelector("#modal__help button[type=button]")
  if(sosSubmit) {
    sosSubmit.addEventListener("click", setSos)
    sosSubmit.addEventListener("touchstart", setSos)
    sosSubmit.addEventListener("mousedown", setSos)
    sosSubmit.addEventListener("touchend", unsetSos)
    sosSubmit.addEventListener("mouseup", unsetSos)
  }
})

export function updateMap(data, callback=null) {
  console.log(data, callback)
  if(callback) callback(data)
}

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('/service-worker.js', {type:'module'})
  })
}