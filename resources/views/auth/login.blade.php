@extends('layouts.app')
@php
$animation  = " transition duration-600";
$inputStyle = "border-b border-white bg-black outline-none focus:bg-black focus:border-tertiary hover:border-primary text-sm p-2 w-full shadow-sm placeholder-white hover:placeholder-primary text-white";
$labelStyle = "text-white text-lg hover:text-primary-500 cursor-pointer";
@endphp
@section('content')
  <div class="transition duration-300 w-full p-12">
    <form method="POST" action="{{ route('login') }}">
      @csrf
      <div class="flex flex-col items-stretch content-center">
        <legend class="w-full mb-12 text-white">
          <div class="text-3xl font-bold mb-4 {{$animation}}">Sign in</div>
        </legend>
        <fieldset data-prefix="email" class="w-full mb-6">
          <label for="email" class="{{$labelStyle}} {{$animation}}">Email</label>
          <input id="email" class="{{$inputStyle}} {{$animation}}" type="email" placeholder="Enter your email address" name="email" required />
        </fieldset>
        <fieldset data-prefix="password" class="w-full mb-6">
          <label for="password" class="{{$labelStyle}} {{$animation}}">Password</label>
          <input id="password" class="{{$inputStyle}} {{$animation}}" type="password" placeholder="Enter your password" name="password" required/>
        </fieldset>
      </div>
      <nav class="flex flex-col content-stretch items-stretch mt-4">
        <button type="submit" class="w-full outline-none flex flex-col items-center content-center justify-center rounded-full bg-primary hover:bg-white focus:bg-white text-white hover:text-primary focus:text-primary text-lg xl:text-2xl 3xl:text-2xl 4xl:text-4xl text-center font-bold p-2 {{$animation}}">Sign In</button>
      </nav>
    </form>
  </div>
@endsection