@php
  $route_name     = Request::path();
  $route_name     = str_replace('/', ' ', $route_name);
  $user           = Auth::user();
  $btnStyle       = "flex items-center p-2 rounded-full cursor-pointer bg-primary hover:bg-white font-bold text-white hover:text-primary text-base lg:text-xl transition duration-600";
  $btnSVGStyle    = "block rounded-full w-6 lg:w-8 h-6 lg:h-8 bg-gray-50 text-primary border-2 md:border-3 lg:border-4 border-white";
  $btnLabelStyle  = "block px-3 lg:px-4";
  $mainBtnStyle   = "mx-6 mb-8 md:mx-8 lg:mx-12 content-between";
  $modalDivStyle  = "fixed top-0 right-0 " . ( $user ? 'w-2/3' : 'w-screen' ) . " h-full overflow-auto z-40 flex flex-col content-center items-center hidden transition opacity-0 duration-600";
  $modalHStyle    = "order-2 mb-1 lg:mb-3 xl:mb-6 2xl:mb-8 3xl:mb-10 4xl:mb-16 text-lg lg:text-xl xl:text-2xl 2xl:text-3xl 3xl:text-5xl 4xl:text-9xl font-bold text-overlay uppercase";
  $modalSecStyle  = "relative flex flex-col z-20 w-full sm:w-70 xl:w-64 2xl:w-68 3xl:w-70 4xl:w-96 h-full my-12 landscape-mobile:my-auto lg:my-20 xl:my-10 2xl:my-14 3xl:my-16 4xl:my-32 px-12 xs:px-32 lg:px-0 text-gray-50";
  $modalPStyle    = "text-sm xl:text-base 2xl:text-2xl 3xl:text-3xl 4xl:text-7xl leading-4 xl:leading-8 2xl:leading-10";
  $modalP0Style   = "order-3 mb-8 2xl:mb-10 3xl:mb-12 4xl:mb-32 ". $modalPStyle;
  $modalP1Style   = "order-4 mb-12 lg:mb-16 xl:mb-20 2xl:mb-12 3xl:mb-16 4xl:mb-32 ". $modalPStyle;
  $logo__campaign = config('app.fb_url_prefix', './images/') . 'logo__campaign.png' . config('app.fb_url_suffix', '');
  $logo__map      = config('app.fb_url_prefix', './images/') . 'logo__interactive_map.png' . config('app.fb_url_suffix', '');
  $topography     = config('app.fb_url_prefix', './images/') . 'topography.jpg' . config('app.fb_url_suffix', '');
@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="{{$user ? 'cms' : ''}}">
  <head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.description') }}">
    <meta name="msapplication-TileColor" content="{{ config('app.theme_color') }}">
    <meta name="theme-color" content="{{ config('app.theme_color')}}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="{{ config('app.theme_color') }}" />
    <meta name="map-id" content="{{ config('app.map_id') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if ($user)
    <meta name="user" content="{{$user}}">
    @endif
    <x-meta/>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="{{ asset('js/map_functions.js') }}"></script>
    <style>
      .gmnoprint{background:black;color:white;font-weight:normal;text-transform:uppercase}
      .gm-style-cc{height: unset !important;line-height: unset !important;}
      .gm-style-cc>div:first-child{width:unset;height:unset;position:static;display:none}
      .gm-style-cc>div:last-child{padding:0.25rem 0.5rem;font-size:0.5875rem;color:var(--primary)}
      .gm-style-cc button,.gm-style-cc a,.gm-style-cc span{color: var(--primary) !important;}
    </style>
    <title>{{ config('app.name', 'Glenlore Map') }}</title>
  </head>
  <body class="{{$route_name}} w-screen h-full bg-primary--900"{{$user ? 'data-logged' : ''}}>
    <x-sprite/>
    @if ($user)
    <header id="topbar" class="absolute md:fixed top-0 left-0 w-full z-50 px-4 bg-tertiary flex"></header>
    <article id="sidebar" data-fb_prefix="{{ config('app.fb_url_prefix') }}" data-fb_suffix="{{ config('app.fb_url_suffix') }}" class="relative md:fixed top-0 left-0 z-40 h-1/2 md:h-screen pt-24 pb-8 md:pt-10 md:pb-8 px-4 bg-black flex-col flex w-full md:w-2/5"></article>
    @endif
    <main class="relative z-10 flex flex-wrap relative bg-primary-900 w-screen{{ $user ? ' md:w-3/5' : '' }} h-full min-h-screen ml-auto">
      <header class="absolute top-0 left-0 w-full h-28 landscape-mobile:h-14 z-40 hidden flex{{ $user ? ' md:hidden' : '' }} flex-col items-center content-center px-2 landscape-mobile:px-1 transition duration-600 pointer-events-none" style="background:linear-gradient(180deg, #00A9FF 55%, transparent 100%)">
        <img alt="{{ config('app.logo_alt', ' | Glenlore Trails') }}" width="240" height="116" src="{{ config('app.fb_url_prefix', './images/') . 'logo__campaign.png' . config('app.fb_url_suffix', '')}}" class="h-16 landscape-mobile:h-8 lg:h-20 xl:h-28 2xl:h-32 3xl:h-44 4xl:h-88 w-auto mt-2 lg:mt-4 mx-auto"/>
      </header>
      <article class="flex m-0 px-0 items-stretch justify-stretch w-full flex-col">
        <div id="map" class="w-full h-full">
          <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ config('app.map_key') }}&v=beta&channel=2"></script>
        </div>
        <aside class="hidden">
          <button data-tracking="check_location" data-location class="{{$btnStyle}} {{$mainBtnStyle}}">
            <svg class="{{$btnSVGStyle}}">
              <use xlink:href="#icon__location"/>
            </svg>
            <span class="{{$btnLabelStyle}}">Find Me</span>
          </button>
          <button data-tracking="initiate_sos" data-sos class="{{$btnStyle}} {{$mainBtnStyle}}">
            <svg class="{{$btnSVGStyle}}">
              <use xlink:href="#icon__exclamation_point"/>
            </svg>
            <span class="{{$btnLabelStyle}}">SOS</span>
          </button>
        </aside>
      </article>
    </main>
    <div data-modal class="{{$modalDivStyle}}" id="modal__help">
      <x-modal_nav/>
      <form class="relative z-20 flex flex-col content-center items-center my-auto w-64 text-center">
        <legend class="order-2 pointer-events-none uppercase transition">
          <div class="text-gray-50 font-bold">
            <span class="block text-display-3 leading-none">Tap &amp; hold </span>
            <span class="block text-display-6 leading-none">for help</span>
          </div>
          <div class="text-gray-50 text-center hidden">
            <span class="block text-display-0 leading-none font-bold">Signal has </span>
            <span class="block text-display-1 leading-none font-bold mb-4">been sent</span>
            <span class="block text-display-1 leading-none font-bold">Help is on </span>
            <span class="block text-display-2 leading-none font-bold mb-4">the way</span>
            <span class="block text-display-4 leading-none">If this is an </span>
            <span class="block text-display-4 leading-none">emergency </span>
            <span class="block text-display-7 leading-none">please dial 911</span>
          </div>
        </legend>
        <input type="hidden" id="user_location" value="{}"/>
        <button title="Press and Hold for Help" type="button" class="w-32 h-32 relative rounded-full p-2 bg-primary text-gray-50 order-1 mb-12 transition flex items-center justify-center flex-col">
          <svg data-progress class="pointer-events-none absolute h-40 w-30 overflow-visible -top-4 -left-4 transition duration-300" viewBox="0 0 150 150" xmlns="http://www.w3.org/2000/svg">
            <circle stroke-width="8" stroke="#6666FF" fill="transparent" r="71" cx="75" cy="75"/>
          </svg>
          <svg id="exclamation" class="absolute h-24 w-24 pointer-events-none transition duration-300 my-auto" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g transform="translate(-15,-15)">
              <path d="M64.5906 86.8188C59.8461 86.8188 56 90.6649 56 95.4094C56 100.154 59.8461 104 64.5906 104C69.3351 104 73.1812 100.154 73.1812 95.4094C73.1812 90.6649 69.3351 86.8188 64.5906 86.8188Z" fill="white"/>
              <path d="M64.4541 24C70.6034 24 75.4238 29.2827 74.8622 35.4063L71.2223 75.0948C70.9015 78.5933 67.9673 81.2707 64.4541 81.2707V81.2707C60.941 81.2707 58.0068 78.5933 57.6859 75.0948L54.0461 35.4063C53.4845 29.2827 58.3049 24 64.4541 24V24Z" fill="white"/>
            </g>
          </svg>
          <svg id="action_lines" viewBox="0 0 195 84" class="h-40 w-40 absolute -top-28 -left-4 pointer-events-none transition origin-bottom duration-300 delay-300 opacity-0 scale-0" xmlns="http://www.w3.org/2000/svg">
            <path d="M97.4541 0C103.603 0 108.424 5.28271 107.862 11.4063L104.222 51.0948C103.901 54.5933 100.967 57.2707 97.4541 57.2707V57.2707C93.941 57.2707 91.0068 54.5933 90.6859 51.0948L87.0461 11.4063C86.4845 5.28272 91.3049 0 97.4541 0V0Z" fill="#6666FF"/>
            <path d="M9.91957 27.7271C15.245 24.6524 22.0609 26.8172 24.6364 32.4011L41.3284 68.5924C42.7998 71.7825 41.5974 75.5684 38.5549 77.3249V77.3249C35.5124 79.0815 31.6326 78.2299 29.6055 75.3606L6.60907 42.8092C3.06093 37.7869 4.59416 30.8017 9.91957 27.7271V27.7271Z" fill="#6666FF"/>
            <path d="M184.555 27.7271C189.88 30.8017 191.413 37.7869 187.865 42.8092L164.869 75.3606C162.842 78.2299 158.962 79.0815 155.919 77.3249V77.3249C152.877 75.5684 151.675 71.7826 153.146 68.5924L169.838 32.4012C172.413 26.8172 179.229 24.6524 184.555 27.7271V27.7271Z" fill="#6666FF"/>
          </svg>
          <svg id="smiley" class="h-24 w-24 -mb-6 mt-6 pointer-events-none hidden" viewBox="0 0 96 53" fill="none" xmlns="http://www.w3.org/2000/svg">
            <defs>
              <clipPath id="smile_clip">
                <circle class="transition duration-700" style="transform:translate(48px, 50px)" r="1" cx="0" cy="0"/>
              </clipPath>
            </defs>
            <path id="smiley__eye--l" class="transition duration-300 hidden" style="transform:translate(-8px, 0)" d="M87 0C82.0294 0 78 4.02944 78 9C78 13.9706 82.0294 18 87 18C91.9706 18 96 13.9706 96 9C96 4.02944 91.9706 0 87 0Z" fill="white"/>
            <path id="smiley__eye--r" class="transition duration-300 hidden" style="transform:translate(6px, 0)" d="M9 0C4.02944 0 0 4.02944 0 9C0 13.9706 4.02944 18 9 18C13.9706 18 18 13.9706 18 9C18 4.02944 13.9706 0 9 0Z" fill="white"/>
            <path style="clip-path:url(#smile_clip)" d="M16 33C16 33 23.4221 49 48 49C72.5779 49 80 33 80 33" stroke="white" stroke-width="8" stroke-linecap="round"/>
          </svg>
        </button>
      </form>
    </div>
    <div data-modal class="{{$modalDivStyle}}" id="modal__out_of_zone">
      <x-modal_nav/>
      <section class="{{$modalSecStyle}} justify-between text-center">
        <h2 class="{{$modalHStyle}}">Not on the Trail?</h2>
        <p class="{{$modalP0Style}}">It looks like you're not currently on the trail. Feel free to explore without location services.</p>
        <p class="{{$modalP1Style}}">Come back here when you are on the trail to unlock additional features.</p>
        <x-modal_cta/>
        <x-location_icon/>
      </section>
    </div>
    <div data-modal class="{{$modalDivStyle}}" id="modal__geolocation_error">
      <x-modal_nav/>
      <section class="{{$modalSecStyle}} justify-between text-center">
        <h2 class="{{$modalHStyle}}">Location Error!</h2>
        <p class="{{$modalP0Style}}">It looks like you're not currently on the trail or we can't recognize your location. Feel free to explore without location services.</p>
        <p class="{{$modalP1Style}}">Come back here when you are on the trail to unlock additional features.</p>
        <x-modal_cta/>
        <x-location_icon/>
      </section>
    </div>
    <div data-modal class="{{$modalDivStyle}} bg-cover" id="modal__welcome" style="background-image:url({{$topography}});">
      <a data-login href="/login" class="absolute z-20 top-4 right-4 flex p-2 rounded-full cursor-pointer text-primary hover:text-white text-xs transition duration-600">
        <span class="block px-1 lg:px-2">Login</span>
      </a>
      <section class="{{$modalSecStyle}} justify-center">
        <h2 class="flex flex-col text-xl mb-2 landscape-mobile:mb-1 lg:mb-4 uppercase">
          <img class="mt-8 3xl:mt-16 w-full max-w-xs landscape-mobile:max-w-xxs md:max-w-sm h-auto mx-auto" alt="Fate | Glenlore Trails Haunted Forest" src="{{ $logo__campaign }}" style="transform:scale(1.25)"/>
        </h2>
        <img alt="Interactive Map" src="{{ $logo__map }}" class="mb-2 landscape-mobile:mb-0 lg:mb-4 w-full max-w-xs landscape-mobile:max-w-xxs md:max-w-sm h-auto mx-auto"/>
        <p class="mb-6 landscape-mobile:mb-2 lg:mb-8 mx-auto landscape-mobile:mx-16 w-auto max-w-xs md:max-w-sm text-sm landscape-mobile:text-xxs xl:text-base">Our interactive map allows you to track yourself on the trail. Tap a location to learn more about the experience.</p>
        <button data-close title="Close"
         class="{{$btnStyle}} w-full flex-col mx-auto max-w-xs landscape-mobile:w-48 md:max-w-sm landscape-mobile:text-xxs">
          Tap to Begin
        </button>
      </section>
    </div>
  </body>
</html>