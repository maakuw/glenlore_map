<link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.fb_url_prefix', './images/') . 'logo__campaign.png' . config('app.fb_url_suffix', '') }}">
<link rel="icon" type="image/png" sizes="512x512" href="{{ config('app.fb_url_prefix', './images/') . 'icon--512x512.png' . config('app.fb_url_suffix', '') }}">
<link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.fb_url_prefix', './images/') . 'icon--192x192.png' . config('app.fb_url_suffix', '') }}">
<link rel="icon" type="image/png" sizes="128x128" href="{{ config('app.fb_url_prefix', './images/') . 'icon--128x128.png' . config('app.fb_url_suffix', '') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ config('app.fb_url_prefix', './images/') . 'icon--32x32.png' . config('app.fb_url_suffix', '') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ config('app.fb_url_prefix', './images/') . 'icon--16x16.png' . config('app.fb_url_suffix', '') }}">
<link rel="manifest" href="mix-manifest.json">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap" rel="stylesheet">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/core.css') }}" rel="stylesheet">