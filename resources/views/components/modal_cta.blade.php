<button data-tracking="modal_close" data-close title="Close"
 class="flex flex-col items-center content-center justify-center order-5 rounded-full mb-0 landscape-mobile:mb-2 md:max-w-sm mt-auto bg-primary hover:bg-white font-semibold text-white hover:text-primary text-lg landscape-mobile:text-xs lg:text-xl xl:text-2xl 2xl:text-3xl 3xl:text-5xl 4xl:text-8xl w-full p-2 2xl:p-4 3xl:p-6 4xl:p-12 transition duration-600">
  Sounds Good!
</button>