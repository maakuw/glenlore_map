<nav class="fixed top-0 left-0 w-full h-full z-10 bg-overlay-900/80">
  <button data-tracking="modal_close" data-close title="Close"
   class="absolute right-8 2xl:right-12 3xl:right-16 4xl:right-32 top-8 2xl:top-12 3xl:top-16 4xl:top-32 w-12 xl:w-14 2xl:w-16 3xl:w-24 4xl:w-48 h-12 xl:h-14 2xl:h-16 3xl:h-24 4xl:h-48 flex flex-col items-center content-center justify-center rounded-full bg-primary hover:bg-white text-white hover:text-primary text-3xl xl:text-5xl 3xl:text-8xl 4xl:text-10xl text-center transition duration-600"
  >
    &times;
  </button>
</nav>