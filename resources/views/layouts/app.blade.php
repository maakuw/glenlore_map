@php
    $route_name = Request::path();
    $route_name = str_replace('/', ' ', $route_name);
    $logo       = config('app.fb_url_prefix', './images/') . 'logo__campaign.png' . config('app.fb_url_suffix', '');;
    $bg         = config('app.fb_url_prefix', './images/') . 'topography.jpg' . config('app.fb_url_suffix', '');;
@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.description') }}">
    <meta name="msapplication-TileColor" content="#ff00cc">
    <meta name="theme-color" content="{{ config('app.theme_color')}}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <x-meta/>
    <title>Login</title>
  </head>
  <body class="{{$route_name}} w-screen h-screen">
    <x-sprite/>
    <main class="flex nowrap content-stretch items-stretch w-screen h-screen overflow-hidden bg-black">
      <aside class="absolute z-10 flex flex-col relative w-full lg:w-sidebar">
        <figure class="flex flex-col w-full mx-auto my-12 bg-center bg-contain bg-no-repeat" style="background-image:url({{$logo}})">
          <img alt="FATE | Glenlore Trails • Haunted Forest" width="113" height="64" src="{{$logo}}" class="invisible w-full h-32 mx-auto"/>
        </figure>
        @yield('content')
      </aside>
      <article class="relative z-0 lg:flex nowrap relative hidden lg:w-content bg-cover bg-center" style="background-image: url({{$bg}})">
        <img alt="A pattern of curved lines recreating the topography of the area" src="{{$bg}}" class="hidden"/>
      </article>
    </main>
  </body>
</html>