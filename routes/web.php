<?php

namespace App\Http\Resources;
namespace App\Http\Controllers;
//namespace App\Models;

use App\Http\Controllers\RequestsController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

//Global Authorization Routes
Auth::routes();

//Frontend
Route::view('/', 'base');
Route::view('/#activations', 'base')->middleware('auth');
Route::view('/#settings', 'base')->middleware('auth');
Route::view('/#users', 'base')->middleware('auth');
Route::view('/#users/{id}', 'base')->middleware('auth');

//Logout
Route::get('/logout', [UsersController::class, 'logout']);

Route::resources([
    '/resource/settings' => SettingsController::class, 
    '/resource/users' => UsersController::class, 
    '/resource/activations' => ActivationsController::class, 
    '/resource/requests' => RequestsController::class
]);