const colors = require('tailwindcss/colors')
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      gray: colors.gray,
      white: colors.white, 
      black: {
        'DEFAULT': '#000000',
        '900': '#000000'
      },
      primary: {
        'DEFAULT':'#6666FF',
        '700': '#6666FF',
        '800': '#000099',
        '900': '#000066'
      },
      secondary: {
        'DEFAULT': '#73D0FF',
        '700': '#73D0FF', 
        '800': '#73D0FF',
        '900': '#73D0FF'
      },
      tertiary: {
        'DEFAULT': '#CC66FF'
      },
      overlay: {
        '900': '#73D0FF',
        'icon': '#FFFFFF',
        'text': '#FFFFFF'
      },
      header: {
        'DEFAULT':'#6666FF'
      }
    }, 
    dropShadow: {
      'sm': '0 0 1px rgb(0 0 0 / 33%)', 
      'md': '0 0 2px rgb(0 0 0 / 50%)'
    }, 
    fill: {
      current: 'currentColor'
    },
    extend: {
      border: {
        '4': '0.25rem'
      }, 
      fontFamily: {
        'sans': ['Poppins', 'Arial', 'sans-serif']
      }, 
      fontSize: {
        'xxs': ['0.6875rem', {
          lineHeight: '1.125',
        }],
        'display-0': '31px',
        'display-1': '35px',
        'display-2': '40px',
        'display-3': '41px',
        'display-4': '24px',
        'display-5': '50px', 
        'display-6': '52px',
        'display-7': '19.5px',
        '7xl': ['4rem', {
          lineHeight: '1.5',
        }],
        '8xl': '5rem',
        '9xl': ['6rem', {
          lineHeight: '1.25',
        }],
        '10xl': '9rem'
      },
      height: {
        '100': '28rem'
      }, 
      padding: {
        '2': '0.375rem'
      },
      maxWidth: {
        'xxs': '10em', 
        'xl': '30em', 
        '2xl': '40em', 
        '3xl': '64em', 
        '4xl': '82em'
      },
      screens: {
        'landscape-mobile': { 'raw': '(orientation: landscape) and (max-width: 845px)' },
      },
      width: {
        '64': '18.75rem',
        '68': '28rem', 
        '70': '35rem', 
        '72': '40rem',
        '78': '52rem', 
        '80': '58rem',
        '84': '62rem',
        '86': '70rem',
        '96': '75rem',
        '100': '28rem',
        'content': 'calc(100vw - 26.75rem)',
        'sidebar': '26.75rem'
      }
    },
    fontFamily: {
      'sans': ['Poppins', 'Arial', 'sans-serif']
    },
    screens: {
      'xxs': '479px',
      'xs': '599px',
      'sm': '759px',
      'md': '1079px', 
      '3xl': '1920px',
      '4xl': '3200px',
      ...defaultTheme.screens
    }
  },
  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
  content: [
    './resources/views/**/*.php',
    './resources/js/*.vue',
    './resources/js/map_functions.js'
  ], 
  plugins: [], 
  darkMode: 'media',
}