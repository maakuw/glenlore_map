const mix = require('laravel-mix');
require('laravel-mix-workbox');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
const use_maps = process.env.APP_ENV === 'local' ? true : false;
mix.js('resources/js/map_functions.js', 'public/js').vue()
.sass('resources/scss/app.scss', 'public/css')
.copyDirectory('resources/images', 'public/images')
.sourceMaps(use_maps, 'source-map')
.generateSW()
.version();